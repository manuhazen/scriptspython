__author__ = 'manuh'
# -*- encoding: utf-8 -*-
#Agenda Telefonica
#v0.1 por Emmanuel Jiménez

import modules

modules.bienvenida()

opcion = input("> ")

print("Has seleccionado la opcion",opcion)

if opcion == "1":
    modules.escribirAgenda()
elif opcion == "2":
    print("Dime cuantos registros quieres ver:")
    registros = input("> ")
    registrosNumeros = int(registros)
    modules.listarAgenda((registrosNumeros+1))
elif opcion == "3":
    print("Dime el nombre de la persona que buscas")
    nombreABuscar = input(">")
    modules.buscador(nombreABuscar)
else:
    print("La opcion que has seleccionado no es valida")

