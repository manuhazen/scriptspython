__author__ = 'manuh'
# -*- encoding: utf-8 -*-
#Agenda Telefonica
#v0.1 por Emmanuel Jiménez

def bienvenida():
    print("Bienvenidos a Agenda Telefonica")
    print("Selecciona una opcion:")
    print("1 - Añadir un registro a la Agenda")
    print("2 - Lista el contenido de la Agenda")
    print("3 - Buscar por nombre")

def escribirAgenda():
    print("Has elegido añadir un registro a la Agenda")
    nombre = input("Introduce un nombre de contacto")
    telefono = input("Introduce un numero de contacto")

    agenda = open("agendatelefonica.csv")
    for i in range(1,60):
        linea = agenda.readline()
        lineapartida = linea.split(",")
        print(lineapartida[0])
        if lineapartida[0] != "":
            memoria = lineapartida[0]
    print("El numero maximo es ",memoria)
    agenda.close()
    posicion = memoria + 1

    print("Se ha guardado en la genda el contacto: ",nombre,"con el numero",telefono)
    agenda = open("agendatelefonica.csv",'a')
    agenda.write(posicion)
    agenda.write(nombre)
    agenda.write(",")
    agenda.write(telefono)
    agenda.write(",")
    agenda.write("\n")
    agenda.close()

def listarAgenda(fin):
    print("Has elegido listar el contenido de la Agenda")
    agenda = open("agendatelefonica.csv")
    numero = 0
    for i in range(1,fin):
        lectura = agenda.readline()
        print(lectura.replace(",","\t\t"))
        numero = numero + 1
    print("Ya se ha terminado de leer la Agenda")
    agenda.close()

def buscador(nombreABuscar):
    print("Estas son las coincidencias")
    agenda = open("agendatelefonica.csv")
    for i in range(1,30):
        lectura = agenda.readline()
        partir = lectura.split(',')
        if nombreABuscar == partir[1]:
            print(partir[2])
    agenda.close()