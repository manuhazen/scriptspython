__author__ = 'manuh'
# -*- encoding: utf-8 -*-
#Programado en Python
#Utilizando Clases
#Escrito por Emmanuel Jiménez

class Auto(object):
    condicion = "nuevo"
    def __init__(self, modelo, color, kpl):
         self.modelo = modelo
         self.color = color
         self.kpl = kpl

    def verAuto(self):
        return "Este es un %s %s que alcanza %s kpl." % (self.modelo, self.color, self.kpl)

    def manejarAuto(self):
        self.condicion = "usado"

class AutoElectrico(Auto):
    def __init__(self, modelo, color, kpl, tipoDeBateria):
        super(AutoElectrico, self).__init__(modelo, color, kpl)
        self.tipoDeBateria = tipoDeBateria

    def manejarAuto(self):
        self.condicion = "como nuevo"


miAuto = AutoElectrico("Opel", "Black", 33, "sales fundidas")
print (miAuto.condicion)
miAuto.manejarAuto()
print (miAuto.condicion)
print (miAuto)