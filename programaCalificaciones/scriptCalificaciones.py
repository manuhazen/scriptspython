__author__ = 'manuh'
# -*- encoding: utf-8 -*-
#Script para Cálculo de las Calificaciones
#v0.1 por Emmanuel Jiménez

notas = [100, 100, 90, 40, 80, 100, 85, 70, 90, 65, 90, 85, 50.5]

def print_calificaciones(calificaciones):
    for calificacion in calificaciones:
        print (calificacion),

def notas_sum(calificaciones):
    total = 0
    for calificacion in calificaciones:
        total += calificacion
    return total

def promedio_notas(calificaciones):
    suma = notas_sum(calificaciones)
    promedio = suma / float(len(calificaciones))
    return promedio

def varianza_notas(calificaciones):
    promedio = promedio_notas(calificaciones)
    varianza = 0
    total2 = 0
    for i in calificaciones:
        varianza = (promedio - i) ** 2
    total = varianza / notas_sum(calificaciones)
    return total

print (varianza_notas(notas))

def calificaciones_std_desviacion(varianza):
    return varianza ** 0.5

varianza = varianza_notas(notas)
print_calificaciones(notas)
print (calificaciones_std_desviacion(varianza))
print(promedio_notas)