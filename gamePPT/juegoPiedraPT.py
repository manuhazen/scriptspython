__author__ = 'manuh'
# -*-coding:utf-8-*-
# Escrito por Emmanuel Jiménez
# v0.1 Piedra, Papel o Tijeras

import time
from time import sleep
import random

print("Juego de Piedra, Papel o Tijeras")
print("Elige tu opción")

usu="-"*35
opc=["piedra", "papel", "tijeras"]
while True:
    x=input("piedra, papel, tijeras")
    if x not in opc:
        print("No hagas trampas!")
        continue

    comp=random.choice(opc)
    sleep(0.5)
    print(("La Computadora eligió {}.").format(comp))
    if x==comp:
        sleep(0.5)
        print(("\n Empates! \n{}").format(usu))
    elif x=="piedra" and comp=="tijeras":
        sleep(0.5)
        print(("\nTu ganas, Piedra rompe Tijeras\n{}").format(usu))
    elif x=="papel" and comp=="piedra":
        sleep(0.5)
        print(("\nTu ganas, Papel envuelve Piedra\n{}").format(usu))
    elif x=="tijeras" and comp=="papel":
        sleep(0.5)
        print(("\nTu ganas, Tijeras corta Papel\n{}").format(usu))
    else:
        sleep(0.5)
        print(("\n Perdiste. {} le gana a {}\n").format(comp, x, usu))

input(piedra)